<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    //Accion:UN METODO DEL CONTROLLADOR, contienen el codigo a ejecutar
    //Nombre: puede ser cualquiera
    //recomendado el nombre en minuscula
    public function index(){    
        //seleccionar las categorias existentes
        $categorias = Categoria::paginate(5);
        //enviar la coleccion de categorias a una vista
        //y las vamos a mostrar alli
        return view("categorias.index")->with("categorias" , $categorias);
        
    }

    //mostrar el formulario de crear categoria
    public function create(){
        //echo "Formulario de Categoria";
        return view('categorias.new');
    } 

    //llegar los datos desde el formulario
    //guardar la categoria en BD
    public function store(Request $r){
        
        //Validacion
        //1. establecer las reglas de validacion para cada campo
        $reglas = [
            "categoria" => ["required", "alpha" ] 
        ];

        $mensajes = [
            "required" => "Campo Obligatorio", 
            "alpha" => "solo letras"
        ];

        //2. crear el objeto validador
        $validador = Validator::make($r->all(), $reglas, $mensajes);
        
        //3. Validar : metodos fails
        //            retorna true(v) si la validacion falla
        //            retorna falso si la validacion falla
        if ($validador->fails()){
            //codigo cuando falla la validacion
            return redirect("categorias/create")->withErrors($validador);

        }else{
            //codigo cuando la validacion es correcta
        }

        $categoria = new Categoria();
        //asignar el nombre
        $categoria->name = $r->input("categoria");
        //guardar la nueva categoria
        $categoria->save();
        return redirect('categorias/create')->with("mensaje", "Categoria Guardada");
        
    }


    public function edit($category_id){
        //Seleccionar la categoria editar
        $categoria = Categoria::find($category_id);

        //mostrar la vista d eactualizar categoria
        //llevado dentro la categoria
        return view("categorias.edit")->with("categoria", $categoria);
    }

    public function update($category_id){
        //seleccionar la categoria a editar
        $categoria = Categoria::find($category_id);
        //editar atributos
        $categoria->name = $_POST["name"];
        //guardar cambios
        $categoria->save();
        //retornar
        return redirect("categorias/edit/$category_id")->with("mensaje" , "Categoria editada");

    }

    
}