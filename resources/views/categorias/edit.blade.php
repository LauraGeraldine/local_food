<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha256-0XAFLBbK7DgQ8t7mRWU5BF2OMm9tjtfH945Z7TTeNIo=" crossorigin="anonymous" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Añadir Categoría</title>
</head>
<body>
        
        @if(session("mensaje"))
            <p class="alert-success"> {{  session("mensaje")    }} </p>
            
        @endif
        
        

       <form class="form-horizontal" action='{{  url("categoria/update/$categoria->category_id  ") }}'  method="post">
        <fieldset>
        @csrf 

        <!-- Form Name -->
        <legend>Nueva Categoría</legend>

        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="categoria">Nombre Categoría:</label>  
        <div class="col-md-4">
        <input id="categoria" value="{{ $categoria->name }}" name="categoria"type="text" placeholder="" class="form-control input-md">
        <strong class="text-danger">{{  $errors->first("categoria")  }} </strong>
        </div>
        </div>

        <!-- Button -->
        <div class="form-group">
        <label class="col-md-4 control-label" for=""></label>
        <div class="col-md-4">
            <button id="" name="" class="btn btn-primary">Enviar</button>
        </div>
        </div>

        </fieldset>
        </form>
</body>
</html>